package dev.creepah.tp.cmd;

import dev.creepah.tp.RebootTP;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import tech.rayline.core.command.*;

@CommandPermission("reboot.tpdeny")
@CommandMeta(description = "Deny a teleport request")
public final class TpDenyCommand extends RDCommand {

    public TpDenyCommand() {
        super("tpdeny");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        Player target = null;
        if (args.length > 0) {
            target = Bukkit.getPlayer(args[0]);
            if (target == null) throw new NormalCommandException("The player " + args[0] + " could not be found!");
        }

        RebootTP.get().getManager().attemptDeny(player, target);
    }
}
