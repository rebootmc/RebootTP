package dev.creepah.tp.cmd;

import dev.creepah.tp.RebootTP;
import dev.creepah.tp.manager.Request;
import dev.creepah.tp.manager.TeleportManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import tech.rayline.core.command.*;

@CommandPermission("reboot.tpahere")
@CommandMeta(description = "Teleport a player to you")
public final class TpaHereCommand extends RDCommand {

    public TpaHereCommand() {
        super("tpahere");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        if (args.length == 0) throw new ArgumentRequirementException("Invalid arguments! Usage: /tphere <player>");

        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) throw new NormalCommandException("The player " + args[0] + " could not be found!");

        if (target.getUniqueId().equals(player.getUniqueId()))
            throw new NormalCommandException("You cannot teleport to yourself!");

        TeleportManager manager = RebootTP.get().getManager();
        manager.tpRequest(player, target, Request.Type.TPHERE);
    }
}
