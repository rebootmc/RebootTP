package dev.creepah.tp.cmd;

import dev.creepah.tp.RebootTP;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import tech.rayline.core.command.*;

@CommandPermission("reboot.tpaccept")
@CommandMeta(description = "Accept a teleport request")
public final class TpAcceptCommand extends RDCommand {

    public TpAcceptCommand() {
        super("tpaccept");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        Player target = null;
        if (args.length > 0) {
            target = Bukkit.getPlayer(args[0]);
            if (target == null)
                throw new NormalCommandException("The player " + args[0] + " could not be found!");
        }

        RebootTP.get().getManager().attemptAccept(player, target);
    }
}
