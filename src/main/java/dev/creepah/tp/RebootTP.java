package dev.creepah.tp;

import dev.creepah.tp.cmd.TpAcceptCommand;
import dev.creepah.tp.cmd.TpDenyCommand;
import dev.creepah.tp.cmd.TpaCommand;
import dev.creepah.tp.cmd.TpaHereCommand;
import dev.creepah.tp.manager.TeleportManager;
import lombok.Getter;
import tech.rayline.core.plugin.RedemptivePlugin;
import tech.rayline.core.plugin.UsesFormats;

@UsesFormats
public final class RebootTP extends RedemptivePlugin {

    private static RebootTP instance;
    public static RebootTP get() {
        return instance;
    }

    @Getter private TeleportManager manager;

    @Override
    protected void onModuleEnable() throws Exception {
        instance = this;
        manager = new TeleportManager();

        registerCommand(new TpaCommand());
        registerCommand(new TpaHereCommand());
        registerCommand(new TpAcceptCommand());
        registerCommand(new TpDenyCommand());
    }
}
