package dev.creepah.tp.manager;

import dev.creepah.tp.RebootTP;
import lombok.Data;
import org.bukkit.entity.Player;

@Data
public final class Request {

    private final Player from; // Who sent the request.
    private final Player to; // Who will have to accept the request.
    private final Type type;

    public void accept() {
        TeleportManager manager = RebootTP.get().getManager();

        if (type == Type.TPHERE) {
            manager.teleport(to, from);
            from.sendMessage(RebootTP.get().formatAt("accepted").get());
        } else {
            manager.teleport(from, to);
            to.sendMessage(RebootTP.get().formatAt("accepted").get());
        }
    }

    public void deny() {
        RebootTP plugin = RebootTP.get();

        from.sendMessage(plugin.formatAt("denied-by").with("player", to.getName()).get());
        to.sendMessage(plugin.formatAt("denied").with("player", from.getName()).get());
    }

    public boolean online() {
        return from != null && from.isOnline() && to != null && to.isOnline();
    }

    public enum Type {
        TPHERE("tphere"), TPA("tpa");

        private String string;

        Type(String str) {
            string = str;
        }

        public String getString() {
            return string;
        }
    }
}
