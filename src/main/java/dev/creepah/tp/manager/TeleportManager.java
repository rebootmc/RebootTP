package dev.creepah.tp.manager;

import dev.creepah.tp.RebootTP;
import lombok.Getter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import tech.rayline.core.command.NormalCommandException;
import tech.rayline.core.util.RunnableShorthand;

import java.util.*;

public final class TeleportManager {

    private FileConfiguration config = RebootTP.get().getConfig();
    @Getter private List<Request> requests = new ArrayList<>();
    private List<UUID> waitingPlayers = new ArrayList<>();
    private int waitTime = config.getInt("wait", 5);

    public TeleportManager() {
        RebootTP.get().observeEvent(PlayerMoveEvent.class)
                .filter(event -> waitingPlayers.contains(event.getPlayer().getUniqueId()))
                .subscribe(event -> {
                    Player player = event.getPlayer();
                    if (event.getTo().getX() != event.getFrom().getX() && event.getTo().getZ() != event.getFrom().getZ()) {
                        waitingPlayers.remove(player.getUniqueId());
                        player.sendMessage(RebootTP.get().formatAt("teleport-cancelled").get());
                   }
                });

        RebootTP.get().observeEvent(PlayerQuitEvent.class)
                .filter(event -> waitingPlayers.contains(event.getPlayer().getUniqueId()))
                .subscribe(event -> waitingPlayers.remove(event.getPlayer().getUniqueId()));
    }

    // Teleport a player to another player, with a cooldown.
    public void teleport(Player teleporter, Player to) {
        RebootTP plugin = RebootTP.get();

        waitingPlayers.add(teleporter.getUniqueId());
        teleporter.sendMessage(plugin.formatAt("teleporting-in").with("time", waitTime).with("player", to.getName()).get());

        RunnableShorthand.forPlugin(plugin).with(() -> {
            if (waitingPlayers.contains(teleporter.getUniqueId()) && to.isOnline()) {
                teleporter.teleport(to);
                teleporter.sendMessage(plugin.formatAt("teleported").get());
            }
            waitingPlayers.remove(teleporter.getUniqueId());

        }).later(20 * waitTime);
    }

    // Request to teleport to/here the specified player.
    public void tpRequest(Player from, Player to, Request.Type type) throws NormalCommandException {
        if (getAwaitingRequest(from, to) != null)
            throw new NormalCommandException("You've already sent a request to " + to.getName() + "!");

        RebootTP plugin = RebootTP.get();

        Request request = new Request(from, to, type);
        requests.add(request);
        from.sendMessage(plugin.formatAt("request-sent").with("player", to.getName()).get());
        to.sendMessage(plugin.formatAt(type.getString() + "-request").with("player", from.getName()).get());

        RunnableShorthand.forPlugin(plugin).with(() -> {
            if (getAwaitingRequest(from, to) != null) {
                requests.remove(request);
                if (from.isOnline()) from.sendMessage(plugin.formatAt("request-timeout").with("player", to.getName()).get());
            }
        }).later(20 * 30);
    }

    // Attempt to ACCEPT a request potentially involving the provided target player.
    public void attemptAccept(Player player, Player target) throws NormalCommandException {
        Request request = getAwaitingRequest(player, target);
        if (request != null) {
            requests.remove(request);
            if (!request.online()) throw new NormalCommandException("The sender of the request is no longer online!");
            request.accept();

        } else throw new NormalCommandException("You have no waiting teleport request!");
    }

    // Attempt to DENY a request potentially involving the provided target player.
    public void attemptDeny(Player player, Player target) throws NormalCommandException {
        Request request = getAwaitingRequest(player, target);
        if (request != null) {
            requests.remove(request);
            if (!request.online()) throw new NormalCommandException("The sender of the request is no longer online!");
            request.deny();

        } else throw new NormalCommandException("You have no waiting teleport request!");
    }

    public Request getAwaitingRequest(Player player, Player target) {
        for (Request request : requests) {
            if (target == null) {
                if (request.getTo().equals(player)) return request;
            } else {
                if (request.getTo().equals(player) && request.getFrom().equals(target)) return request;
            }
        }

        return null;
    }
}
